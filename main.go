package main

import (
	"flag"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	_ "gitlab.com/T4cC0re/gitlab-cli/importer"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
	"log"
	"os"
)

var conf *config.Config
var version = "source"
var C = flag.String("C", "", "chdir before running")
var help = flag.Bool("help", false, "show this help")
var showVersion = flag.Bool("version", false, "show version and exit")

func init() {
	var err error
	var file string
	flag.Parse()
	if *showVersion {
		showBanner()
		os.Exit(0)
	}
	if *help {
		showHelp()
		os.Exit(0)
	}
	if *C != "" {
		err = os.Chdir(*C)
		if err != nil {
			log.Fatalln(err)
		}
	}

	if len(flag.Args()) < 1 {
		showHelp()
		os.Exit(1)
	}
	if flag.Args()[0] == "manpage" {
		registrar.GenManpage(version)
		os.Exit(0)
	}

	file, err = config.Find("")
	if err != nil {
		log.Fatalln(err)
	}
	conf, err = config.Read(file)
	if err != nil {
		log.Fatalln(err)
	}
}

func showBanner() {
	// Snatched from https://gitlab.com/gitlab-org/gitlab-development-kit/blob/e9c044ebe303a478756f4c135b0b0e98e0deae0b/run
	fmt.Print("\n" +
		"          \033[38;5;88m`                        `\n" +
		"         :s:                      :s:\n" +
		"        `oso`                    `oso.\n" +
		"        +sss+                    +sss+\n" +
		"       :sssss:                  -sssss:\n" +
		"      `ossssso`                `ossssso`\n" +
		"      +sssssss+                +sssssss+\n" +
		"     -ooooooooo-++++++++++++++-ooooooooo-\n" +
		"    \033[38;5;208m`:/\033[38;5;202m+++++++++\033[38;5;88mosssssssssssso\033[38;5;202m+++++++++\033[38;5;208m/:`\n" +
		"    -///\033[38;5;202m+++++++++\033[38;5;88mssssssssssss\033[38;5;202m+++++++++\033[38;5;208m///-\n" +
		"   .//////\033[38;5;202m+++++++\033[38;5;88mosssssssssso\033[38;5;202m+++++++\033[38;5;208m//////.\n" +
		"   :///////\033[38;5;202m+++++++\033[38;5;88mosssssssso\033[38;5;202m+++++++\033[38;5;208m///////:\n" +
		"    .:///////\033[38;5;202m++++++\033[38;5;88mssssssss\033[38;5;202m++++++\033[38;5;208m///////:.`\n" +
		"      `-://///\033[38;5;202m+++++\033[38;5;88mosssssso\033[38;5;202m+++++\033[38;5;208m/////:-`\n" +
		"         `-:////\033[38;5;202m++++\033[38;5;88mosssso\033[38;5;202m++++\033[38;5;208m////:-`\n" +
		"            .-:///\033[38;5;202m++\033[38;5;88mosssso\033[38;5;202m++\033[38;5;208m///:-.\n" +
		"              `.://\033[38;5;202m++\033[38;5;88mosso\033[38;5;202m++\033[38;5;208m//:.`\n" +
		"                 `-:/\033[38;5;202m+\033[38;5;88moo\033[38;5;202m+\033[38;5;208m/:-`\n" +
		"                    `-++-`\033[0m\n",
	)
	fmt.Printf("GitLab-CLI\nVersion %s\n", version)
}

func main() {
	switch flag.Args()[0] {
	case "help", "--help":
		showHelp()
		os.Exit(0)
	default:
		err := registrar.Call(conf, flag.Args()[0], flag.Args()[1:])
		if err != nil {
			fmt.Fprintf(os.Stderr, "\nAn error has ocurred. '%s'\n", err.Error())
			os.Exit(1)
		}
	}
}

func showHelp() {
	showBanner()
	fmt.Printf("Usage: %s [<global flags>] <subcommand/namespace> [<namespace command>] [<subcommand flags>]\n", os.Args[0])
	fmt.Println("\nglobal flags:")
	flag.PrintDefaults()
	registrar.List()
}
