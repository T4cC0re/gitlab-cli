package my

import (
	"flag"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
	"strings"
)

func init() {
	namespace := registrar.RegisterNamespace("my", "Provides a federated view of your issues, MRs, todos, etc.")
	namespace.RegisterCommand("issues",
		true,
		execIssues,
		"Show assigned issues",
		"",
		"-closed\tshow closed issues (hidden by default)",
		"-confidential\tshow confidential issues (hidden by default)",
	)
	namespace.RegisterCommand("todos",
		true,
		execTodos,
		"Show open todos",
		"",
		"-done\tshow done items instead of pending ones",
	)
	// TODO: Show MRs
}

func truncateString(str string, num int) string {
	if len(str) > num {
		if num > 3 {
			num -= 3
		}
		return str[0:num] + "..."
	} else {
		return str
	}
}

func execTodos(conf *config.Config, args []string) (err error) {
	pFlags := flag.NewFlagSet("my_issues", flag.ExitOnError)
	done := pFlags.Bool("done", false, "show done items instead of pending ones")
	_ = pFlags.Parse(args)

	for _, instance := range conf.Instances {
		fmt.Printf("--- My todos on %s ---\n", instance.Alias)
		var todos []*helper.Todo
		todos, err = helper.GetTodos(&instance, *done)

		for _, todo := range todos {
			fmt.Printf("(%s) %s: %s (%s)\n", todo.State, todo.Project.NameWithNamespace, truncateString(strings.SplitN(todo.Body, "\n", 2)[0], 60), todo.TargetURL)
		}
	}

	return
}

func execIssues(conf *config.Config, args []string) (err error) {
	//TODO: Offer more options, such as showing issues opened by me and offer a flag to show confidential issues

	pFlags := flag.NewFlagSet("my_issues", flag.ExitOnError)
	closed := pFlags.Bool("closed", false, "show closed issues (hidden by default)")
	confidential := pFlags.Bool("confidential", false, "show confidential issues (hidden by default)")
	_ = pFlags.Parse(args)

	for _, instance := range conf.Instances {
		fmt.Printf("--- Issues assigned to me on %s ---\n", instance.Alias)
		var issues []*helper.Issue
		issues, err = helper.GetAssignedIssues(&instance)

		for _, issue := range issues {
			if issue.Confidential && !*confidential {
				fmt.Printf("Confidential issue. %s\n", issue.WebURL)
				continue
			}
			if issue.State == "closed" && !*closed {
				continue
			}

			var project *helper.Project
			project, err = helper.ProjectByID(&instance, issue.ProjectId)
			if err != nil {
				return
			}
			fmt.Printf("(%s) %s #%d: %s (%s)\n", issue.State, project.NameWithNamespace, issue.IID, issue.Title, issue.WebURL)
		}
	}

	return
}
