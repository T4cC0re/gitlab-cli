package pipeline

import (
	"bufio"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

func showLogs(instance *config.Instance, projectId int64, pipelineIDs []int64) (err error) {
	var jobs []helper.Job
	for _, pipelineID := range pipelineIDs {
		var tmp []helper.Job
		tmp, err = helper.JobsByPipeline(instance, projectId, pipelineID)
		if err != nil {
			return
		}
		jobs = append(jobs, tmp...)
	}

	for _, job := range jobs {
		err = showJobLog(instance, projectId, job)
		if err != nil {
			return
		}
	}

	return
}

func jobFinishedOrFailed(job helper.Job) bool {
	return job.Status != "running" && job.Status != "pending" && job.Status != "created"
}

func tailLogs(instance *config.Instance, projectId int64, pipelineIDs []int64) (err error) {
	var jobs []helper.Job
	for _, pipelineID := range pipelineIDs {
		var tmp []helper.Job
		tmp, err = helper.JobsByPipeline(instance, projectId, pipelineID)
		if err != nil {
			return
		}
		jobs = append(jobs, tmp...)
	}

	wg := sync.WaitGroup{}
	for _, job := range jobs {
		// If the job is done, ignore. -tail should only show unfinished jobs.
		if jobFinishedOrFailed(job) {
			continue
		}

		wg.Add(1)
		go func(job helper.Job) {
			cancelJob := make(chan interface{})

			// Handler to stop tailing once done
			go func() {
				for {
					time.Sleep(5 * time.Second)
					var new_job helper.Job
					new_job, err = helper.GetJob(instance, projectId, job.ID)
					if err != nil {
						continue
					}
					if jobFinishedOrFailed(new_job) {
						cancelJob <- struct{}{}
						return
					}
				}
			}()

			err = tailJobLog(instance, projectId, job, cancelJob)
			if err != nil {
				_, _ = fmt.Fprintf(os.Stderr, "%s", err.Error())
			}
			wg.Done()
		}(job)
	}

	wg.Wait()

	return
}

func showJobLog(instance *config.Instance, projectId int64, job helper.Job) (err error) {
	var resp *http.Response

	endpoint := fmt.Sprintf("projects/%d/jobs/%d/trace", projectId, job.ID)
	resp, err = helper.GETEndpointRaw(instance, endpoint)
	if err != nil {
		return
	}
	if resp.StatusCode > 299 {
		_ = resp.Body.Close()
		fmt.Printf("%s (%d)\t | No log available\n", job.Name, job.ID)
	} else {
		sc := bufio.NewScanner(resp.Body)
		var text string
		for sc.Scan() {
			text = strings.TrimRight(sc.Text(), "\n\r\x00")
			fmt.Printf("%s (%d)\t | %s\n", job.Name, job.ID, text)
		}
		_ = resp.Body.Close()
		if err := sc.Err(); err != nil {
			return err
		}
	}

	return
}

/**
tailJobLog tails a single job log

There is no way to get an incremental plain-text log from the GitLab API.
This is why we HEAD and see if there is new content, if so, we fetch it
and discard the bytes already read.
*/
func tailJobLog(instance *config.Instance, projectId int64, job helper.Job, cancel chan interface{}) (err error) {
	var req *http.Request
	var resp *http.Response
	var bytesRead int64
	var shouldStop bool

	sleepBetweenFetches := time.Second * 2

	useRangedRequests := helper.InstanceSupportsFeature(instance, "job-trace-with-range")
	endpoint := fmt.Sprintf("projects/%d/jobs/%d/trace", projectId, job.ID)

	// Make sure we can stop
	go func() {
		<-cancel
		shouldStop = true
	}()

	for {
		if shouldStop {
			break
		}

		req, err = helper.NewAuthenticatedAPIRequest(instance, "HEAD", endpoint, nil)
		if err != nil {
			return
		}
		resp, err = http.DefaultClient.Do(req)
		if err != nil {
			return
		}
		_ = resp.Body.Close()
		if resp.ContentLength <= bytesRead {
			time.Sleep(sleepBetweenFetches)
			continue
		}

		req, err = helper.NewAuthenticatedAPIRequest(instance, "GET", endpoint, nil)
		if err != nil {
			return
		}
		if useRangedRequests {
			req.Header.Add("Range", fmt.Sprintf("bytes=%d-", bytesRead))
		}
		resp, err = http.DefaultClient.Do(req)
		if err != nil {
			return
		}

		if resp.StatusCode > 299 && resp.StatusCode != 416 {
			_ = resp.Body.Close()
			fmt.Printf("%s (%d)\t | No log available\n", job.Name, job.ID)
			break
		} else {
			buf := bufio.NewReader(resp.Body)
			if !useRangedRequests {
				_, _ = buf.Discard(int(bytesRead))
			}
			sc := bufio.NewScanner(buf)
			var text string
			for sc.Scan() {
				text = sc.Text()
				bytesRead += int64(len(text) + 1)
				text = strings.Trim(text, "\n\r\x00")
				fmt.Printf("%s (%d)\t | %s\n", job.Name, job.ID, text)
			}

			_ = resp.Body.Close()
			if err := sc.Err(); err != nil {
				return err
			}
		}
		time.Sleep(sleepBetweenFetches)
	}

	return
}
