package pipeline

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
	"strings"
)

var ErrNoJobId = errors.New("no job id or job name given")
var ErrNoPipelines = errors.New("no active pipelines found")

func init() {
	registrar.Alias("pipe", "pipeline")
	ns := registrar.RegisterNamespace("pipeline", "view pipelines and act upon them")
	ns.RegisterCommand(
		"logs",
		false,
		execLogs,
		"show runner logs for current pipeline",
		"",
		"-tail, -follow\tto follow unfinished jobs",
		"-remote\tuse latest commit in tracking branch instead of latest local commit",
	)
	ns.RegisterCommand(
		"status",
		true,
		execStatus,
		"show pipeline status for the current commit",
		"",
		"-remote\tuse latest commit in tracking branch instead of latest local commit",
	)
	ns.RegisterCommand(
		"trigger",
		false,
		execTrigger,
		"trigger a manual job",
		"",
		"-job\tjob id to trigger",
		"-name\tjob name to trigger",
		"-remote\tuse latest commit in tracking branch instead of latest local commit",
	)
	ns.RegisterCommand(
		"retry",
		false,
		execRetry,
		"retry a job",
		"",
		"-job\tjob id to retry",
		"-name\tjob name to retry",
		"-remote\tuse latest commit in tracking branch instead of latest local commit",
	)
	ns.RegisterCommand(
		"cancel",
		false,
		execCancel,
		"cancel a job",
		"",
		"-job\tjob id to cancel",
	)
	ns.RegisterCommand(
		"run",
		false,
		execRun,
		"run a new pipeline",
		"",
		"-ref\trun on this ref instead of the current branch",
		"-json\tuse this JSON object as pipeline variables",
		"-envfile\tuse variables in this envfile as pipeline variables",
		"-var\tset this variable on the pipeline (can be specified multiple times)",
	)
}

func maxIntSlice(v []int64) int64 {
	sort.Slice(v, func(i, j int) bool { return v[i] < v[j] })
	return v[len(v)-1]
}

func execTrigger(conf *config.Config, args []string) (err error) {
	return execCommon(conf, args, helper.TriggerManualJob, true)
}

func execRetry(conf *config.Config, args []string) (err error) {
	return execCommon(conf, args, helper.RetryJob, true)
}

func execCancel(conf *config.Config, args []string) (err error) {
	return execCommon(conf, args, helper.CancelJob, false)
}

type arrayFlags []string

func (i *arrayFlags) String() string {
	return ""
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func setVar(vars *map[string]string, pair string) {
	splits2 := strings.SplitN(string(pair), "=", 2)
	if len(splits2) == 2 {
		(*vars)[splits2[0]] = strings.Trim(splits2[1], " \t")
	} else {
		(*vars)[splits2[0]] = ""
	}
}

func execRun(conf *config.Config, args []string) (err error) {
	var cmdVars arrayFlags
	vars := map[string]string{}
	pFlags := flag.NewFlagSet("pipeline_run", flag.ExitOnError)
	ref := pFlags.String("ref", "", "run on this ref instead of the current branch")
	cmdJson := pFlags.String("json", "", "use this JSON object as pipeline variables")
	envfile := pFlags.String("envfile", "", "use variables in this envfile as pipeline variables")
	pFlags.Var(&cmdVars, "var", "set this variable on the pipeline (can be specified multiple times)")
	_ = pFlags.Parse(args)

	var instance *config.Instance
	var project *helper.Project
	instance, project, _, err = boilerplate(conf, false)
	if err != nil {
		return
	}

	if *ref == "" {
		*ref, err = helper.CurrentBranch()
		if err != nil {
			return
		}
	}

	if *cmdJson != "" {
		err = json.Unmarshal([]byte(*cmdJson), &vars)
		if err != nil {
			return
		}
	}

	if *envfile != "" {
		var buf []byte

		buf, err = ioutil.ReadFile(*envfile)
		if err != nil {
			return
		}

		splits := strings.Split(string(buf), "\n")
		for _, line := range splits {
			line = strings.Trim(line, " \t")
			if len(line) == 0 {
				continue
			}

			setVar(&vars, line)
		}
	}

	for _, line := range cmdVars {
		setVar(&vars, line)
	}

	var pipeline helper.PipelineRequest
	for k, v := range vars {
		pipeline.Variables = append(pipeline.Variables, helper.PipelineVariable{
			Key:          k,
			VariableType: "env_var",
			Value:        v,
		})
	}
	pipeline.Ref = *ref

	type inlinePipeline struct {
		ID  int64  `json:"id"`
		SHA string `json:"sha"`
	}

	var data inlinePipeline
	var resp *http.Response
	resp, err = helper.NewAuthenticatedJSONAPIRequest(instance, "POST", fmt.Sprintf("projects/%d/pipeline", project.ID), pipeline, &data)

	if resp.StatusCode > 299 {
		fmt.Fprintf(os.Stderr, "API Error %d (%s)\n", resp.StatusCode, resp.Header.Get("x-request-id"))
		return helper.ErrAPIError
	}

	fmt.Printf("Successfully launched pipeline #%d (%s/pipelines/%d)\n", data.ID, project.WebURL, data.ID)
	return nil
}

func execCommon(conf *config.Config, args []string, trigger func(instance *config.Instance, projectId int64, jobId int64) (job *helper.Job, err error), useName bool) (err error) {
	pFlags := flag.NewFlagSet("pipeline_trigger", flag.ExitOnError)
	jobId := pFlags.Int64("job", 0, "job id to trigger/retry/cancel")
	var jobName *string
	var remote *bool
	if useName == true {
		jobName = pFlags.String("name", "", "job name to trigger/retry")
		remote = pFlags.Bool("remote", false, "use latest commit in tracking branch instead of latest local commit")
	} else {
		phJobName := ""
		jobName = &phJobName
		phRemote := false
		remote = &phRemote
	}
	_ = pFlags.Parse(args)

	instance, project, commit, err := boilerplate(conf, *remote)
	if err != nil {
		return
	}

	if *jobName != "" {
		var pipelineIDs []int64
		var jobs []helper.Job

		pipelineIDs, err = helper.PipelinesMatchingCommit(instance, project.ID, commit)
		if err != nil {
			return
		}
		if len(pipelineIDs) < 1 {
			return ErrNoPipelines
		}
		jobs, err = helper.JobsByPipeline(instance, project.ID, maxIntSlice(pipelineIDs))
		if err != nil {
			return
		}
		*jobId = helper.FindJobIdByName(jobs, *jobName)
	}

	if *jobId == 0 {
		return ErrNoJobId
	}

	var job *helper.Job
	job, err = trigger(instance, project.ID, *jobId)
	if err != nil {
		return
	}
	fmt.Printf("Job now in %s state!\n", job.Status)
	return
}

func execStatus(conf *config.Config, args []string) (err error) {
	pFlags := flag.NewFlagSet("pipeline_status", flag.ExitOnError)
	remote := pFlags.Bool("remote", false, "use latest commit in tracking branch instead of latest local commit")
	_ = pFlags.Parse(args)

	instance, project, commit, err := boilerplate(conf, *remote)
	if err != nil {
		return
	}

	var pipelineIDs []int64
	pipelineIDs, err = helper.PipelinesMatchingCommit(instance, project.ID, commit)
	if err != nil {
		return
	}

	fmt.Printf("Current pipelines for commit %+v\n", pipelineIDs)
	return showStatus(instance, project.ID, pipelineIDs)

}

func execLogs(conf *config.Config, args []string) (err error) {
	pFlags := flag.NewFlagSet("pipeline_logs", flag.ExitOnError)
	tail := pFlags.Bool("tail", false, "Tail logs from runners")
	follow := pFlags.Bool("follow", false, "Tail logs from runners")
	remote := pFlags.Bool("remote", false, "use latest commit in tracking branch instead of latest local commit")
	_ = pFlags.Parse(args)

	var pipelineIDs []int64
	instance, project, commit, err := boilerplate(conf, *remote)
	if err != nil {
		return
	}
	pipelineIDs, err = helper.PipelinesMatchingCommit(instance, project.ID, commit)
	if err != nil {
		return
	}

	fmt.Printf("Current pipelines for commit %+v\n", pipelineIDs)

	if *tail || *follow {
		return tailLogs(instance, project.ID, pipelineIDs)
	} else {
		return showLogs(instance, project.ID, pipelineIDs)
	}
}

func boilerplate(conf *config.Config, fetchRemote bool) (instance *config.Instance, project *helper.Project, commit string, err error) {
	var root, projectPath string
	root, err = helper.GitRoot()
	if err != nil {
		return
	}

	if fetchRemote {
		commit, err = helper.CurrentCommitHashOnTrackingUpstream()
		if err != nil {
			return
		}
		fmt.Printf("Current commit on tracking branch: %s\n", commit)
	} else {
		commit, err = helper.CurrentCommitHash()
		if err != nil {
			return
		}
		fmt.Printf("Current local commit: %s\n", commit)
	}
	instance, projectPath, err = helper.InstanceForRepo(conf, root)
	if err != nil {
		return
	}
	if !helper.InstanceSupportsFeature(instance, "pipelines") {
		err = helper.ErrFeature
		return
	}
	project, err = helper.ProjectByPath(instance, projectPath)
	if err != nil {
		return
	}
	fmt.Printf("Detected GitLab instance '%s', project '%s', project: %d\n", instance.Alias, project.NameWithNamespace, project.ID)
	return
}
