package pipeline

import (
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"sort"
)

func showStatus(instance *config.Instance, projectId int64, pipelineIDs []int64) (err error) {
	for _, pipelineID := range pipelineIDs {
		fmt.Printf(" --- Pipeline #%d ---\n", pipelineID)
		var stages = map[string][]helper.Job{}
		var data []helper.Job
		endpoint := fmt.Sprintf("projects/%d/pipelines/%d/jobs", projectId, pipelineID)
		_, err = helper.NewAuthenticatedJSONAPIRequest(instance, "GET", endpoint, nil, &data)
		if err != nil {
			return
		}

		// Because of the go runtime, iteration over a map does not guarantee the same order.
		// We keep a ordered slice of stage names to deal with this.
		// See: https://blog.golang.org/go-maps-in-action#TOC_7.
		stageNames := make([]string, 0)

		for _, job := range data {
			if _, ok := stages[job.Stage]; !ok {
				stages[job.Stage] = []helper.Job{}
				stageNames = append(stageNames, job.Stage)
			}
			stages[job.Stage] = append(stages[job.Stage], job)
		}

		sort.Strings(stageNames)

		for _, stage := range stageNames {
			fmt.Printf("> %s:\n", stage)
			sort.Slice(stages[stage], func(a, b int) bool {
				return stages[stage][a].Name < stages[stage][b].Name
			})
			for _, job := range stages[stage] {
				fmt.Printf(">> %s (%d):\t%s\n", job.Name, job.ID, helper.ColorStatus(job.Status))
			}
		}
	}
	return
}
