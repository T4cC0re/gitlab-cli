package lint

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
	"io/ioutil"
	"net/http"
	"os"
	"path"
)

func init() {
	registrar.RegisterSubCommand(
		"lint",
		exec,
		"lint the .gitlab-ci.yml in the current repository",
	)
}

func exec(conf *config.Config, args []string) (err error) {
	var root string
	var instance *config.Instance
	root, err = helper.GitRoot()
	if err != nil {
		return err
	}
	var ciYml []byte
	pathToYml := path.Join(root, ".gitlab-ci.yml")
	/**
	 * #nosec G304
	 * This reads the CI YML to be linted via the API.
	 */
	ciYml, err = ioutil.ReadFile(pathToYml)
	if err != nil {
		return
	}

	instance, _, err = helper.InstanceForRepo(conf, root)
	if err != nil {
		return err
	}
	if !helper.InstanceSupportsFeature(instance, "ci-lint") {
		return helper.ErrFeature
	}

	type inline struct {
		Content string `json:"content"`
	}

	var payload inline
	payload.Content = string(ciYml)
	j, err := json.Marshal(payload)
	if err != nil {
		return
	}

	bytes.NewReader(j)

	var req *http.Request
	var resp *http.Response
	req, err = helper.NewAuthenticatedAPIRequest(instance, "POST", "ci/lint", bytes.NewReader(j))
	if err != nil {
		return
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	type inlineResp struct {
		Status string   `json:"status"`
		Errors []string `json:"errors"`
		Error  string   `json:"error"`
	}

	j, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var response inlineResp
	err = json.Unmarshal(j, &response)
	if err != nil {
		return
	}

	if response.Status == "valid" {
		fmt.Printf("%s is valid\n", pathToYml)

		os.Exit(0)
	} else {
		fmt.Printf("%s is %s\n", pathToYml, response.Status)

		for _, error := range response.Errors {
			fmt.Printf("> %s\n", error)
		}

		errorcount := len(response.Errors)
		if response.Error != "" {
			fmt.Printf("> %s\n", response.Error)
			errorcount++
		}
		os.Exit(errorcount)
	}
	return
}
