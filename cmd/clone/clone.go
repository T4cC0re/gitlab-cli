package clone

import (
	"errors"
	"flag"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"regexp"
	"sort"
	"strings"
)

type Project struct {
	SSHURL            string `json:"ssh_url_to_repo"`
	NameWithNamespace string `json:"name_with_namespace"`
	PathWithNamespace string `json:"path_with_namespace"`
	Mirror            bool   `json:"mirror"`
	Id                int    `json:"id"`
	Message           string `json:"message"`
}

func init() {
	registrar.RegisterSubCommand("clone",
		from,
		"Perform a clone from the provided resource",
		"",
		"-basedir\tclone into another basedir (default: ~/repos)",
		"-allow-mirrors\tallow clones of mirrored projects. By default projects configured with pull mirroring are not cloned.",
	)
}

func from(conf *config.Config, args []string) (err error) {
	home, err := os.UserHomeDir()
	pFlags := flag.NewFlagSet("clone_from", flag.ExitOnError)
	basedir := pFlags.String("basedir", path.Join(home, "repos"), "clone into this basedir")
	cloneMirror := pFlags.Bool("allow-mirrors", false, "allow clones of mirrored projects")
	_ = pFlags.Parse(args)
	for _, item := range pFlags.Args() {
		err = fromItem(conf, *basedir, item, *cloneMirror)
		if err != nil {
			return
		}
	}

	return
}

func doClone(fullpath string, item string, displayName string) (err error) {
	if _, err = os.Stat(fullpath); !os.IsNotExist(err) {
		log.Printf("Project '%s' exists in '%s'...\n", displayName, fullpath)
		return nil
	}
	log.Printf("Cloning '%s' to '%s'...\n", displayName, fullpath)

	var args []string
	switch true {
	case helper.IsGitVersionGreaterOrEqual("2.13"):
		args = []string{"clone", "--recurse-submodules", "-j8"}
	case helper.IsGitVersionGreaterOrEqual("2.8"):
		args = []string{"clone", "--recursive", "-j8"}
	case helper.IsGitVersionGreaterOrEqual("1.6.5"):
		args = []string{"clone", "--recursive"}
	default:
		args = []string{"clone"}
	}
	args = append(args, "--progress", item, fullpath)

	// #nosec G204
	cmd := exec.Command("git", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err = cmd.Start()
	if err != nil {
		return
	}
	err = cmd.Wait()
	if cmd.ProcessState.ExitCode() != 0 {
		log.Printf("An error occurred during cloning: %s\n", err.Error())
	}
	return
}

func fromItem(conf *config.Config, basedir string, item string, cloneMirror bool) (err error) {
	//var instance *config.Instance
	var re = regexp.MustCompile(`(?m)^(?:[a-z]+@|https?:\/\/)([^\/:]+)(?:[:\/])(.*?)(?:\.git)?$`)

	if re.MatchString(item) && (strings.HasSuffix(item, ".git") || strings.HasPrefix(item, "git@")) {
		// Is git URL, no API magic needed.
		itemPath := re.ReplaceAllString(item, basedir+"/$1/$2")
		return doClone(itemPath, item, item)
	}

	matches := re.FindAllStringSubmatch(item, -1)
	if len(matches) != 1 {
		return errors.New("Invalid path")
	}

	var hostname = matches[0][1]
	var uriPath = matches[0][2]

	instance, err := helper.InstanceForHostname(conf, hostname)
	if err != nil {
		return err
	}

	log.Printf("Got instance: %s\n", instance.Alias)

	okay, err := tryProject(instance, basedir, uriPath, cloneMirror)
	if okay {
		// This was a project, not a group
		return
	}
	err = nil

	_, err = tryNamespace(instance, basedir, uriPath, cloneMirror)
	return
}

func parseNext(linkHeader string) string {
	var re = regexp.MustCompile(`(?m)<([^;]+)>\w?;\W?rel="next"`)
	matches := re.FindAllStringSubmatch(linkHeader, -1)
	if len(matches) != 1 {
		return ""
	}

	return matches[0][1]
}

func tryNamespace(instance *config.Instance, basedir string, uriPath string, cloneMirror bool) (okay bool, err error) {
	type inline struct {
		ID   int    `json:"id"`
		Kind string `json:"kind"`
	}

	log.Printf("Checking namespace type...")
	var namespaceObj inline
	_, err = helper.NewAuthenticatedJSONAPIRequest(instance, "GET", fmt.Sprintf("namespaces/%s", url.QueryEscape(uriPath)), nil, &namespaceObj)
	if err != nil {
		return
	}
	var endpoint string
	var pagination string
	switch namespaceObj.Kind {
	case "user":
		endpoint = "users"
		pagination = "offset"
	case "group":
		endpoint = "groups"
		pagination = "keyset"
	default:
		return false, errors.New("no such namespace")
	}

	log.Printf("Gathering projects...")
	var requestURL = fmt.Sprintf("/api/v4/%s/%s/projects?include_subgroups=true&pagination=%s&per_page=20", endpoint, url.QueryEscape(uriPath), pagination)
	var projects []Project
	var resp *http.Response
	for requestURL != "" {
		var chunk []Project
		resp, err = helper.NewAuthenticatedJSONRequest(instance, "GET", requestURL, nil, &chunk)
		if err != nil {
			return
		}
		projects = append(projects, chunk...)
		requestURL = parseNext(resp.Header.Get("Link"))
	}

	sort.SliceStable(projects, func(i, j int) bool { return projects[i].NameWithNamespace < projects[j].NameWithNamespace })

	log.Printf("Cloning projects...")
	errorcount := 0
	for _, project := range projects {
		if project.Mirror && !cloneMirror {
			log.Printf("Project '%s' is a mirror. Provide --allow-mirrors to force cloning mirrors\n", project.NameWithNamespace)
			continue
		}
		err = doClone(path.Join(basedir, strings.ToLower(instance.Host), project.PathWithNamespace), project.SSHURL, project.NameWithNamespace)
		if err != nil {
			// Got logged in doClone
			errorcount++
			err = nil
		}
	}

	if errorcount > 0 {
		return false, errors.New(fmt.Sprintf("there where %d errors while cloning.", errorcount))
	}

	return true, err
}

func tryProject(instance *config.Instance, basedir string, uriPath string, cloneMirror bool) (okay bool, err error) {
	var project Project

	_, err = helper.NewAuthenticatedJSONAPIRequest(instance, "GET", "projects/"+url.QueryEscape(uriPath), nil, &project)
	if err != nil {
		return
	}

	if project.Message != "" {
		return false, errors.New(project.Message)
	}

	if project.Mirror && !cloneMirror {
		log.Printf("Project '%s' is a mirror. Provide --allow-mirrors to force cloning mirrors\n", project.NameWithNamespace)
		return true, nil
	}
	err = doClone(path.Join(basedir, strings.ToLower(instance.Host), project.PathWithNamespace), project.SSHURL, project.NameWithNamespace)
	if err == nil {
		return true, nil
	} else {
		return false, err
	}
}
