package instances

import (
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
)

func init() {
	registrar.Alias("ls", "instances")
	registrar.RegisterSubCommand(
		"instances",
		exec,
		"show configured GitLab instances",
	)
}

func exec(conf *config.Config, args []string) (err error) {
	var user *helper.User

	for _, instance := range conf.Instances {
		fmt.Printf("%s:\n", instance.Alias)
		fmt.Printf(" Host:\t\t%s\n", instance.Host)
		if len(instance.AltHosts) > 0 {
			for _, altHost := range instance.AltHosts {
				fmt.Printf(" AltHost:\t%s\n", altHost)
			}
		}
		user, err = helper.CurrentUser(&instance)
		if err != nil {
			fmt.Printf("Could not get current user, token possibly invalid.\n")
		} else {
			fmt.Printf(" User\t\t%s (%s | %d)\n", user.UserName, user.Name, user.Id)
		}
	}
	if len(conf.Instances) == 0 {
		fmt.Printf("No GitLab instances configured.\n")
	}
	return
}
