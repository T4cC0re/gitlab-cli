package repo

import (
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
)

func init() {
	registrar.Alias("project", "repo")
	namespace := registrar.RegisterNamespace("repo", "show and edit repository")
	namespace.RegisterCommand("info",
		true,
		showInfo,
		"Show information about the current repo",
	)
}

func showInfo(conf *config.Config, args []string) (err error) {
	var root, projectPath string
	var instance *config.Instance
	root, err = helper.GitRoot()
	if err != nil {
		return err
	}
	instance, projectPath, err = helper.InstanceForRepo(conf, root)
	if err != nil {
		return err
	}
	project, err := helper.ProjectByPath(instance, projectPath)
	if err != nil {
		return err
	}

	fmt.Printf(
		"Filesystem:\t%s\nProject:\t%s (%s)\nProject ID:\t%d\nVisibility:\t%s\nWeb URL:\t%s\nClone:\t\t%s (SSH)\t%s (HTTP(S))\nDefault Branch:\t%s\n",
		root,
		project.NameWithNamespace,
		project.PathWithNamespace,
		project.ID,
		project.Visibility,
		project.WebURL,
		project.SSHURL,
		project.HTTPURL,
		project.DefaultBranch,
	)
	return

}
