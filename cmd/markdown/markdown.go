package markdown

import (
	"flag"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
)

func init() {
	registrar.Alias("md", "markdown")
	registrar.RegisterSubCommand(
		"markdown",
		execRender,
		"render markdown",
		"",
		"-in\tmarkdown to render (required)",
		"-out\twrite html output to file",
		"-preview\topen output with this command (if -out is not specified, this will be a temporary file)",
	)
}

type MarkdownRequest struct {
	Text    string `json:"text"`
	GFM     bool   `json:"gfm,omitempty"`
	Project string `json:"project,omitempty"`
}

type MarkdownResponse struct {
	HTML string `json:"html"`
}

func execRender(conf *config.Config, args []string) (err error) {
	pFlags := flag.NewFlagSet("markdown", flag.ExitOnError)
	in := pFlags.String("in", "", "markdown to render")
	out := pFlags.String("out", "", "write html output to file")
	preview := pFlags.String("preview", "", "open output with this command (if -out is not specified, this will be a temporary file)")
	_ = pFlags.Parse(args)

	var instance *config.Instance
	var project *helper.Project
	var root, projectPath string
	root, err = helper.GitRoot()
	if err != nil {
		return
	}
	instance, projectPath, err = helper.InstanceForRepo(conf, root)
	if err != nil {
		return
	}
	if !helper.InstanceSupportsFeature(instance, "markdown") {
		err = helper.ErrFeature
		return
	}

	project, err = helper.ProjectByPath(instance, projectPath)
	if err != nil {
		return
	}

	var markdown string
	var buf []byte
	if buf, err = ioutil.ReadFile(*in); err == nil {
		markdown = string(buf)
	} else {
		return
	}

	var mdReq = MarkdownRequest{
		Text:    markdown,
		GFM:     true,
		Project: project.PathWithNamespace,
	}

	var response MarkdownResponse
	_, err = helper.NewAuthenticatedJSONAPIRequest(instance, "POST", "markdown", mdReq, &response)
	if err != nil {
		return
	}

	var fd *os.File
	var fdOpen bool
	if *out != "" {
		fd, err = os.OpenFile(*out, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600)
		fdOpen = true
	} else if *out == "" && *preview != "" {
		fd, err = ioutil.TempFile("", "*.html")
		fdOpen = true
	} else {
		fd = os.Stdout
	}
	if fd == nil || err != nil {
		return
	}

	// Fix lazy loading for plantuml etc.
	var re = regexp.MustCompile(`(?m)<img src="data:image\/gif;base64,[A-Za-z0-9]+[=]{0,2}"\s+class="lazy"\s+data-src="(http.+)">`)
	response.HTML = re.ReplaceAllString(response.HTML, `<img src="$1">`)

	_, err = fmt.Fprint(fd, response.HTML)
	if err != nil {
		return
	}
	_ = fd.Sync()

	filename := fd.Name()

	if fdOpen {
		_ = fd.Close()
	}

	if *preview != "" {
		/**
		 * #nosec G204
		 * This launches your browser to preview a markdown render
		 */
		err = exec.Command(*preview, filename).Start()
		if err != nil {
			return
		}
	}

	return
}
