package lint

import (
	"flag"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

func init() {
	registrar.Alias("call", "request")
	registrar.Alias("curl", "request")
	registrar.RegisterSubCommand(
		"request",
		exec,
		"perform curl-like authenticated requests against GitLab. The instance will be detected from the passed URL.",
		"",
		"-get\tPerform a GET request (default)",
		"-head\tPerform a HEAD request",
		"-post\tPerform a POST request",
		"-delete\tPerform a DELETE request",
		"-H\tAdd a request header (can be specified multiple times)",
		"-i\tShow response headers plus content",
		"-I\tShow response headers only",
		"-d\tPOST payload as string (prefix @ to load file, e.g. @/foo/bar)",
	)
}

// loosely based on https://stackoverflow.com/questions/28322997
type headerFlag map[string]string

func (i *headerFlag) String() string {
	return fmt.Sprintf("%+v", *i)
}

func (i *headerFlag) Set(in string) error {
	var key, value string

	splits := strings.SplitN(in, ":", 2)
	key = splits[0]
	if len(splits) > 1 {
		value = splits[1]
	}

	(*i)[key] = value
	return nil
}

var headers headerFlag

func exec(conf *config.Config, args []string) (err error) {
	headers = headerFlag{}
	pFlags := flag.NewFlagSet("request", flag.ExitOnError)
	get := pFlags.Bool("get", false, "Perform a GET request (default)")
	head := pFlags.Bool("head", false, "Perform a HEAD request")
	post := pFlags.Bool("post", false, "Perform a POST request")
	delete := pFlags.Bool("delete", false, "Perform a DELETE request")
	pFlags.Var(&headers, "H", "Add a request header (can be specified multiple times)")
	i := pFlags.Bool("i", false, "Show response headers with content")
	I := pFlags.Bool("I", false, "Show response headers only")
	d := pFlags.String("d", "", "POST payload as string (prefix @ to load file, e.g. @/foo/bar)")
	_ = pFlags.Parse(args)

	var method string
	var payload io.Reader
	switch {
	case *head:
		method = "HEAD"
	case *delete:
		method = "DELETE"
	case *post:
		method = "POST"
		payload, err = getPayloadReader(*d)
		if err != nil {
			return
		}
	case *get:
		method = "GET"
	default:
		method = "GET"
	}

	if len(pFlags.Args()) < 1 {
		log.Fatal("Need to provide a URL")
	}

	url := pFlags.Args()[0]
	instance, path, err := helper.InstanceForURL(conf, url)
	if err != nil {
		return
	}

	var req *http.Request
	var resp *http.Response

	req, err = helper.NewAuthenticatedRequest(instance, method, path, payload)
	if err != nil {
		return
	}
	for header, value := range headers {
		req.Header.Add(header, value)
	}
	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	defer resp.Body.Close()

	switch {
	case *I:
		err = resp.Header.Write(os.Stdout)
		if err != nil {
			return
		}
	case *i:
		err = resp.Header.Write(os.Stdout)
		if err != nil {
			return
		}
		_, err = io.Copy(os.Stdout, resp.Body)
		if err != nil {
			return
		}
		fmt.Println()
	default:
		_, err = io.Copy(os.Stdout, resp.Body)
		if err != nil {
			return
		}
		fmt.Println()
	}
	return
}

func getPayloadReader(s string) (io.Reader, error) {
	if strings.HasPrefix(s, "@") {
		file, err := os.Open(s[1:])
		return file, err
	} else {
		return strings.NewReader(s), nil
	}

	return nil, nil

}
