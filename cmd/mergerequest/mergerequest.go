package mergerequest

import (
	"errors"
	"flag"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"gitlab.com/T4cC0re/gitlab-cli/helper"
	"gitlab.com/T4cC0re/gitlab-cli/registrar"
	"os"
	"strings"
)

var ErrMRTargetEqualsSource = errors.New("merge request target branch equals source branch")

func init() {
	registrar.Alias("mr", "mergerequest")
	namespace := registrar.RegisterNamespace("mergerequest", "view, create and edit merge requests for the current repository")
	namespace.RegisterCommand("new",
		true,
		newMR,
		"Create a new merge request",
		"",
		"-from\tuse this branch as source instead of current branch",
		"-to\tuse this branch as target instead of project default",
		"-title\tuse this title for the MR instead of the headline of the last local commit",
		"-description\tuse this description for the MR instead of the message of the last local commit",
		"-default\tuse the default description for the MR instead of the massage of the last local commit",
		"-editor\topen $EDITOR to create MR title and description. (first line: title, blank line, following lines: description). When no title or description is set, this is the default",
		"-noconfirm\tCreate the MR without confirmation",
		"-push\tPush the branch in addition to creating a merge request",
	)
}

func newMR(conf *config.Config, args []string) (err error) {
	var root, projectPath, commit string
	var instance *config.Instance
	var project *helper.Project
	root, err = helper.GitRoot()
	if err != nil {
		return
	}
	commit, err = helper.CurrentCommitHash()
	if err != nil {
		return
	}
	instance, projectPath, err = helper.InstanceForRepo(conf, root)
	if err != nil {
		return
	}
	if !helper.InstanceSupportsFeature(instance, "merge-request") {
		err = helper.ErrFeature
		return
	}
	project, err = helper.ProjectByPath(instance, projectPath)
	if err != nil {
		return
	}

	pFlags := flag.NewFlagSet("mergerequest", flag.ExitOnError)
	from := pFlags.String("from", "", "use this branch as source instead of current branch")
	to := pFlags.String("to", "", "use this branch as target instead of project default")
	title := pFlags.String("title", "", "use this title for the MR instead of the headline of the last local commit")
	description := pFlags.String("description", "", "use this title for the MR instead of the message of the last local commit")
	useDefault := pFlags.Bool("default", false, "use the default description for the MR instead of the massage of the last local commit")
	editor := pFlags.Bool("editor", false, "open $EDITOR to create MR title and description. (first line: title, blank line, following lines: description). When no title or description is set, this is the default")
	noconfirm := pFlags.Bool("noconfirm", false, "Create the MR without confirmation")
	push := pFlags.Bool("push", true, "Push the branch in addition to creating a merge request")
	_ = pFlags.Parse(args)

	if *from == "" {
		*from, err = helper.CurrentBranch()
		if err != nil {
			return
		}
	}
	if *to == "" {
		*to = project.DefaultBranch
	}
	if *to == *from {
		return ErrMRTargetEqualsSource
	}
	var defaultTitle = false
	if *title == "" {
		*title, err = helper.CommitSubject(commit)
		if err != nil {
			return
		}
		defaultTitle = true
	}

	// Deliberately not triggering the defaultDescription flag, because this is an opt-in commandline flag
	if *useDefault {
		*description = (*instance).DefaultDescription
	}

	var defaultDescription = false
	if *description == "" {
		*description, err = helper.CommitBody(commit)
		if err != nil {
			return
		}
		// If the commit does not have a description, use the default one from the config
		if *description == "" {
			*description = (*instance).DefaultDescription
		}
		defaultDescription = true
	}

	merged := fmt.Sprintf("%s\n\n%s", *title, *description)
	if *editor || (defaultTitle && defaultDescription) {
		merged, err = helper.EditInEditor(merged)
	}
	splits := strings.SplitN(merged, "\n", 3)
	*title = splits[0]
	if len(splits) == 3 {
		*description = splits[2]
	} else {
		*description = ""
	}

	mr := &helper.MergeRequest{
		SourceBranch: *from,
		TargetBranch: *to,
		Title:        *title,
		Description:  *description,
	}

	fmt.Printf("source branch:\t%s\ntarget branch:\t%s\npush branch:\t%t\ntitle:\t\t%s\ndescription:\n--------\n%s\n--------\nContinue? (y/N) ", mr.SourceBranch, mr.TargetBranch, *push, mr.Title, mr.Description)

	if !*noconfirm {
		if !helper.Confirm() {
			_, _ = fmt.Fprintln(os.Stderr, "Aborted merge request creation")
			return
		}
	}

	if *push {
		var output string
		output, err = helper.Git("push", "origin", mr.SourceBranch)
		if err != nil {
			return
		}
		_, _ = os.Stderr.WriteString(output)
		_ = os.Stderr.Sync()
	}

	mrr, err := helper.CreateMergeRequest(instance, project.ID, mr)
	if err != nil {
		return
	}
	fmt.Printf("Created Merge request!\nId:\t%d\nURL:\n%s\n", mrr.IId, mrr.WebURL)
	return
}
