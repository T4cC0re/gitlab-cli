#!/usr/bin/env bash

BUILDDIR="/tmp/gitlabclibuild/"
TARGETUSER=1000
TARGETGROUP=1000
OLDPWD=$(pwd)
IMAGE_TAG="registry.gitlab.com/t4cc0re/gitlab-cli:source"

rm -rf ${BUILDDIR}
rm -rf /tmp/gitlab-cli-source.tar

go build .

mkdir -p ${BUILDDIR}bin/
mv gitlab-cli ${BUILDDIR}bin/

cp Dockerfile ${BUILDDIR}
chown -R ${TARGETUSER}:${TARGETGROUP} ${BUILDDIR}
cd ${BUILDDIR}

sudo buildah build-using-dockerfile --no-cache --squash -t ${IMAGE_TAG} .
sudo buildah push ${IMAGE_TAG} docker-archive:${BUILDDIR}/gitlab-cli-source.tar:${IMAGE_TAG}

sudo chown ${TARGETUSER}:${TARGETGROUP} ${BUILDDIR}/gitlab-cli-source.tar

docker load -i ${BUILDDIR}/gitlab-cli-source.tar
docker run --rm -it ${IMAGE_TAG} --version

cd ${OLDPWD}
