package config

import (
	"errors"
	"github.com/ghodss/yaml"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var ErrNoConfig = errors.New("could not find config")

type Instance struct {
	Host     string   `json:"host,omitempty"`
	Token     string   `json:"token,omitempty"`
	Alias              string   `json:"alias,omitempty"`
	AltHosts           []string `json:"alt_hosts,omitempty"`
	DefaultDescription string `json:"default_description,omitempty"`
}

type Config struct {
	Instances []Instance `json:"instances,omitempty"`
	DefaultDescription string `json:"default_description,omitempty"`

}

func isRoot(path string) bool {
	return strings.EqualFold(filepath.Clean(path), filepath.VolumeName(path)+string(filepath.Separator))
}

func Find(path string) (configFile string, err error) {
	var files []os.FileInfo

	if path == "" {
		path, err = os.Getwd()
	}
	files, err = ioutil.ReadDir(path)
	if err != nil {
		return "", err
	}

	for _, file := range files {
		if strings.EqualFold(file.Name(), ".gitlab-cli.yml") {
			configFile = filepath.Join(path, file.Name())
		}
	}

	if configFile == "" {
		if isRoot(path) {
			return loadFallbackConfig()
		} else {
			path = filepath.Join(path, "..")
			return Find(path)
		}
	}
	return
}

func loadFallbackConfig() (configFile string, err error) {
	var home string
	if home = os.Getenv("HOME"); len(home) == 0 {
		return "", ErrNoConfig
	}

	fallbackConfig := filepath.Join(home, ".gitlab-cli.yml")
	stat, err := os.Stat(fallbackConfig)
	if err != nil || stat.IsDir() {
		return "", ErrNoConfig
	}

	return fallbackConfig, nil
}

func Read(path string) (*Config, error) {
	var conf Config

	// #nosec G304 This loads the config file.
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return &Config{}, err
	}

	err = yaml.Unmarshal(data, &conf)
	if err != nil {
		return &Config{}, err
	}

	if conf.DefaultDescription == "" {
		conf.DefaultDescription = "Merge Request generated via [GitLab-CLI](https://gitlab.com/T4cC0re/gitlab-cli)."
	}

	// In case the user does not have an alias set, use the hostname
	// Also set the default MR description to the config wide default, if not set
	for i := range conf.Instances {
		if conf.Instances[i].Alias == "" {
			conf.Instances[i].Alias = conf.Instances[i].Host
		}
		if conf.Instances[i].DefaultDescription == "" {
			conf.Instances[i].DefaultDescription = conf.DefaultDescription
		}
	}

	return &conf, nil
}
