FROM docker.io/library/alpine:3
LABEL maintainer="Nils Bartels <gitlab@bartels.xyz>"

ENV TINI_VERSION=v0.18.0
ENV USER=gitlab-cli
ENV HOME="/home/gitlab-cli"
ENV UID=1234
ENV GID=1234

# Install tini
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-amd64 /sbin/tini

# Add a dedicated user
RUN addgroup -g ${GID} -S ${USER} && \
    adduser -S ${USER} -G ${USER} -u $UID -s /bin/ash

# Fix musl compability
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

# Install git
RUN apk add git openssh-client

# Add gitlab-cli
ADD ./bin/gitlab-cli /usr/bin/gitlab-cli

# Make sure executables are executable
RUN chmod +x /usr/bin/gitlab-cli /sbin/tini
USER ${USER}

ENTRYPOINT ["/sbin/tini", "--", "/usr/bin/gitlab-cli"]
