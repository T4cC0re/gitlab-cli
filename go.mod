module gitlab.com/T4cC0re/gitlab-cli

go 1.12

require (
	github.com/ghodss/yaml v1.0.0
	github.com/hashicorp/go-version v1.2.0
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
