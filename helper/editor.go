package helper

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

func EditInEditor(source string) (out string, err error) {
	var file *os.File
	file, err = ioutil.TempFile(os.TempDir(), "gitlab-cli-*")
	if err != nil {
		_ = os.Remove(file.Name())
		return
	}
	_, err = file.Write([]byte(source))
	if err != nil {
		_ = os.Remove(file.Name())
		return
	}
	err = file.Sync()
	if err != nil {
		_ = os.Remove(file.Name())
		return
	}
	err = file.Close()
	if err != nil {
		_ = os.Remove(file.Name())
		return
	}

	editor := os.Getenv("EDITOR")
	if editor == "" {
		editor = "vi"
	}
	/**
	 * #nosec G204
	 * This launches your $EDITOR to edit MR details.
	 * This can be used to inject commands, but only if the attacker controls your environment.
	 * Accepted risk.
	 */
	cmd := exec.Command(editor, file.Name())
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Start()
	if err != nil {
		_ = os.Remove(file.Name())
		return
	}
	err = cmd.Wait()
	if err != nil {
		_ = os.Remove(file.Name())
		return
	}

	o, err := ioutil.ReadFile(file.Name())
	_ = os.Remove(file.Name())
	return string(bytes.TrimRight(o, "\n\t ")), err
}

/**
 * Returns true if the user confirmed, false otherwise.
 * You need to print the query string yourself!
 */
func Confirm() bool {
	var s string

	_, err := fmt.Scanln(&s)
	if err != nil {
		return false
	}

	s = strings.TrimSpace(s)
	s = strings.ToLower(s)

	return strings.HasPrefix(s, "y")
}
