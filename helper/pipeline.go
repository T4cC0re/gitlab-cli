package helper

import (
	"errors"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"strings"
)

type Job struct {
	ID     int64  `json:"id"`
	Name   string `json:"name"`
	Stage  string `json:"stage"`
	Status string `json:"status"`
}

type PipelineVariable struct {
	Key          string `json:"key"`
	VariableType string `json:"variable_type"`
	Value        string `json:"value"`
}

type PipelineRequest struct {
	Ref       string             `json:"ref"`
	Variables []PipelineVariable `json:"variables"`
}

var ErrNotManual = errors.New("job to be triggered is not a manual job or already running")

func FindJobIdByName(jobs []Job, name string) int64 {
	for _, job := range jobs {
		if strings.EqualFold(job.Name, name) {
			return job.ID
		}
	}
	return 0
}

func JobsByPipeline(instance *config.Instance, projectId int64, pipelineID int64) (jobs []Job, err error) {
	var data []Job
	_, err = NewAuthenticatedJSONAPIRequest(instance, "GET", fmt.Sprintf("projects/%d/pipelines/%d/jobs", projectId, pipelineID), nil, &data)
	if err != nil {
		return
	}
	jobs = append(jobs, data...)
	return
}

func TriggerManualJob(instance *config.Instance, projectId int64, jobId int64) (job *Job, err error) {
	return TriggerJob(instance, projectId, jobId, "play")
}

func RetryJob(instance *config.Instance, projectId int64, jobId int64) (job *Job, err error) {
	return TriggerJob(instance, projectId, jobId, "retry")
}

func CancelJob(instance *config.Instance, projectId int64, jobId int64) (job *Job, err error) {
	return TriggerJob(instance, projectId, jobId, "cancel")
}

func TriggerJob(instance *config.Instance, projectId int64, jobId int64, endpoint string) (job *Job, err error) {
	tmpJob, err := GetJob(instance, projectId, jobId)
	if err != nil {
		return
	}

	if tmpJob.Status != "manual" && endpoint == "play" {
		return nil, ErrNotManual
	}

	_, err = NewAuthenticatedJSONAPIRequest(instance, "POST", fmt.Sprintf("projects/%d/jobs/%d/%s", projectId, jobId, endpoint), nil, &job)
	return
}

func GetJob(instance *config.Instance, projectId int64, jobId int64) (job Job, err error) {
	_, err = NewAuthenticatedJSONAPIRequest(instance, "GET", fmt.Sprintf("projects/%d/jobs/%d", projectId, jobId), nil, &job)
	return
}

/**
PipelinesMatchingCommit returns a slice of matching pipeline IDs.
*/
func PipelinesMatchingCommit(instance *config.Instance, projectId int64, commit string) (pipelines []int64, err error) {
	if instance == nil || projectId == 0 || commit == "" {
		err = ErrNoInstance
		return
	}
	type inlinePipelines struct {
		ID  int64  `json:"id"`
		SHA string `json:"sha"`
	}

	var data []inlinePipelines

	_, err = NewAuthenticatedJSONAPIRequest(instance, "GET", fmt.Sprintf("projects/%d/pipelines", projectId), nil, &data)

	if err != nil {
		return
	}

	for _, pipeline := range data {
		if strings.EqualFold(pipeline.SHA, commit) {
			pipelines = append(pipelines, pipeline.ID)
		}
	}

	return
}
