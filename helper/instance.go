package helper

import (
	"bufio"
	"errors"
	semver "github.com/hashicorp/go-version"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var ErrNoInstance = errors.New("no instance matched")

var featureMap = map[string]string{
	"ci-lint":               "8.12",
	"issues":                "9.5",  // 9.5+ support required fields
	"issues_api_snake_case": "11.0", // Scopes were renames
	"job-trace-with-range":  "99.0", // Currently broken. https://gitlab.com/gitlab-org/gitlab-ce/issues/65045
	"markdown":              "11.0",
	"merge-request":         "9.5",
	"pipelines":             "8.11",
}

var versionCache = map[string]string{}

var ErrFeature = errors.New("required feature is not supported by the affected GitLab instance")

func InstanceVersion(instance *config.Instance) (version string, err error) {
	type inlineVersion struct {
		Version string `json:"version"`
	}
	var data inlineVersion

	_, err = NewAuthenticatedJSONAPIRequest(instance, "GET", "version", nil, &data)
	if err != nil {
		return
	}
	version = data.Version
	versionCache[instance.Host] = version
	return
}

func InstanceSupportsFeature(instance *config.Instance, features ...string) bool {
	version, err := InstanceVersion(instance)
	if err != nil {
		return false
	}

	var featuresAvailable int

	for _, feature := range features {
		if featureVersion, ok := featureMap[feature]; ok {
			gitlab := semver.Must(semver.NewVersion(version))
			feature := semver.Must(semver.NewVersion(featureVersion))
			if gitlab.GreaterThanOrEqual(feature) {
				featuresAvailable++
			}
		}
	}

	return featuresAvailable == len(features)
}

/**
InstanceForRepo returns the instance from the configuration that is applicable fot the repository passed in.
*/
func InstanceForRepo(conf *config.Config, repoPath string) (*config.Instance, string, error) {
	var re = regexp.MustCompile(`(?Um)url\s?=\s?(?:git@|https?://)([^:/]+)(?::|/)(.+)(?:\.git)?$`)

	/**
	 * #nosec G304
	 * This loads the repo's gitconfig
	 */
	f, err := os.Open(filepath.Join(repoPath, ".git", "config"))
	if err != nil {
		return nil, "", err
	}
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		if matches := re.FindStringSubmatch(sc.Text()); len(matches) > 1 {
			for _, instance := range conf.Instances {
				if strings.EqualFold(instance.Host, matches[1]) {
					return &instance, matches[2], nil
				}

				for _, host := range instance.AltHosts {
					if strings.EqualFold(host, matches[1]) {
						return &instance, matches[2], nil
					}
				}
			}
		}
	}
	if err := sc.Err(); err != nil {
		return nil, "", err
	}

	return nil, "", ErrNoInstance
}

/**
InstanceForRepo returns the instance and URL path for the url passed in.
*/
func InstanceForURL(conf *config.Config, url string) (instance *config.Instance, returnUrl string, err error) {
	var re = regexp.MustCompile(`(?Um)^https?://([^/]+)/(.*)$`)

	if matches := re.FindStringSubmatch(url); len(matches) > 2 {
		instance, err = InstanceForHostname(conf, matches[1])
		if err != ErrNoInstance {
			returnUrl = "/" + matches[2]
			return
		}
	}

	return nil, "", ErrNoInstance
}

func InstanceForHostname(conf *config.Config, hostname string) (*config.Instance, error) {
	for _, instance := range conf.Instances {
		if strings.EqualFold(instance.Host, hostname) {
			return &instance, nil
		}

		for _, host := range instance.AltHosts {
			if strings.EqualFold(host, hostname) {
				return &instance, nil
			}
		}
	}
	return nil, ErrNoInstance
}
