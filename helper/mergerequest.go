package helper

import (
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
)

type MergeRequest struct {
	SourceBranch string `json:"source_branch"`
	TargetBranch string `json:"target_branch"`
	Title        string `json:"title"`
	Description  string `json:"description,omitempty"`
}

type MergeRequestResponse struct {
	MergeRequest
	IId    int64  `json:"iid"`
	WebURL string `json:"web_url"`
}

func CreateMergeRequest(instance *config.Instance, projectID int64, mr *MergeRequest) (mrr *MergeRequestResponse, err error) {
	_, err = NewAuthenticatedJSONAPIRequest(instance, "POST", fmt.Sprintf("projects/%d/merge_requests", projectID), mr, &mrr)
	return
}
