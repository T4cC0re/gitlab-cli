package helper

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	semver "github.com/hashicorp/go-version"
)

var ErrGit = errors.New("git command failed")

func Git(arg ...string) (string, error) {
	if os.Getenv("DEBUG") != "" {
		log.Println("git", arg)
	}

	/**
	 * #nosec G204
	 * This runs arbitrary git commands by design
	 */
	cmd := exec.Command("git", arg...)
	ret, err := cmd.Output()
	if err != nil {
		return "", err
	}

	if os.Getenv("DEBUG") != "" {
		log.Println("ret:", string(ret))
	}

	ret = bytes.Trim(ret, "\n\t ")
	return string(ret), nil
}

/**
GitRoot returns the root dir of the current repository
*/
func GitRoot() (string, error) {
	return Git("rev-parse", "--show-toplevel")
}

/**
CurrentCommitHash returns the current commit of the current repository
*/
func CurrentCommitHash() (string, error) {
	return Git("rev-parse", "--verify", "HEAD")
}

/**
CurrentCommitHashOnTrackingUpstream returns the current commit of the current tracking upstream
*/
func CurrentCommitHashOnTrackingUpstream() (string, error) {
	var tracking string
	var err error
	tracking, err = CurrentTrackingBranch()
	if err != nil {
		return "", err
	}

	splits := strings.SplitN(tracking, "/", 2)

	remote := splits[0]
	branch := splits[1]

	url, err := GetRemoteURLByName(remote)
	if err != nil {
		return "", err
	}

	out, err := Git("ls-remote", url, fmt.Sprintf("refs/heads/%s", branch))
	if err != nil {
		return "", err
	}

	return strings.SplitN(out, "\t", 2)[0], nil
}

/**
CommitSubject returns the subject of a commit
*/
func CommitSubject(commithash string) (string, error) {
	path, err := Git("rev-list", "--format=%s", "--max-count=1", commithash)
	if err != nil {
		return "", err
	}
	splits := strings.SplitN(path, "\n", 2)
	if len(splits) != 2 {
		return "", nil
	}
	return splits[1], nil
}

/**
CommitSubject returns the body of a commit
*/
func CommitBody(commithash string) (string, error) {
	path, err := Git("rev-list", "--format=%b", "--max-count=1", commithash)
	if err != nil {
		return "", err
	}
	splits := strings.SplitN(path, "\n", 2)
	if len(splits) != 2 {
		return "", nil
	}
	return splits[1], nil
}

/**
CurrentBranch returns the current branch of the current repository
*/
func CurrentBranch() (string, error) {
	return Git("rev-parse", "--abbrev-ref", "HEAD")
}

func CurrentTrackingBranch() (string, error) {
	return Git("rev-parse", "--abbrev-ref", "--symbolic-full-name", "@{u}")
}

func GetRemoteURLByName(name string) (string, error) {
	return Git("remote", "get-url", name)
}

func Version() (string, error) {
	output, err := Git("--version")
	if err != nil {
		return "", err
	}
	split := strings.Split(output, " ")
	if len(split) >= 3 {
		return split[2], nil
	} else {
		return "", os.ErrNotExist
	}
}

func IsGitVersionGreaterOrEqual(version string) bool {
	gitVersion, err := Version()
	if err != nil {
		return false
	}

	git := semver.Must(semver.NewVersion(gitVersion))
	wanted := semver.Must(semver.NewVersion(version))
	return git.GreaterThanOrEqual(wanted)
}
