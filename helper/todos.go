package helper

import (
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
)

type Todo struct {
	ID      int64 `json:"id"`
	Project struct {
		Id                int64  `json:"id"`
		NameWithNamespace string `json:"name_with_namespace"`
	} `json:"project"` //This is because of stupid API design
	Author     User     `json:"author"`
	ActionName string   `json:"action_name"`
	TargetType string   `json:"target_type"`
	Target     struct{} `json:"target"` //TODO
	TargetURL  string   `json:"target_url"`
	Body       string   `json:"body"`
	State      string   `json:"state"`
	CreatedAt  string   `json:"created_at"`
}

func GetTodos(instance *config.Instance, inDoneState bool) (todos []*Todo, err error) {
	page := 1
	var state string
	if inDoneState {
		state = "done"
	} else {
		state = "pending"
	}

	for {
		var chunk []*Todo
		_, err = NewAuthenticatedJSONAPIRequest(instance, "GET", fmt.Sprintf("todos?state=%s&per_page=100&page=%d", state, page), nil, &chunk)
		if err != nil || len(chunk) == 0 {
			return
		}
		todos = append(todos, chunk...)
		page++
	}

}
