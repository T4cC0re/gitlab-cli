package helper

import (
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"net/url"
)

type Project struct {
	ID                int64  `json:"id"`
	Name              string `json:"name"`
	NameWithNamespace string `json:"name_with_namespace"`
	PathWithNamespace string `json:"path_with_namespace"`
	SSHURL            string `json:"ssh_url_to_repo"`
	HTTPURL           string `json:"http_url_to_repo"`
	WebURL            string `json:"web_url"`
	Visibility        string `json:"visibility"`
	DefaultBranch     string `json:"default_branch"`
	Archived          bool   `json:"archived"`
}

var cache = map[string]*Project{}

/**
ProjectByPath returns the project of a project path/namespace.
*/
func ProjectByPath(instance *config.Instance, projectPath string) (project *Project, err error) {
	if instance == nil || projectPath == "" {
		err = ErrNoInstance
		return
	}

	endpoint := fmt.Sprintf("projects/%s", url.QueryEscape(projectPath))

	var ok bool
	if project, ok = cache[endpoint]; ok {
		return
	}

	var data Project
	_, err = NewAuthenticatedJSONAPIRequest(instance, "GET", endpoint, nil, &data)
	if err != nil {
		return
	}

	cache[endpoint] = &data

	return &data, nil
}

/**
ProjectByID returns the project of a project id.
*/
func ProjectByID(instance *config.Instance, projectId int64) (project *Project, err error) {
	return ProjectByPath(instance, fmt.Sprintf("%d", projectId))

}
