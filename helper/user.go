package helper

import (
	"gitlab.com/T4cC0re/gitlab-cli/config"
)

type User struct {
	Id       uint   `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	UserName string `json:"username,omitempty"`
	WebURL   string `json:"web_url,omitempty"`
}

/**
CurrentUser returns the currently logged in user.
*/
func CurrentUser(instance *config.Instance) (project *User, err error) {
	_, err = NewAuthenticatedJSONAPIRequest(instance, "GET", "user", nil, &project)
	return
}
