package helper

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

var ErrAPIError = errors.New("API error")

func NewAuthenticatedAPIRequest(instance *config.Instance, method string, endpoint string, body io.Reader) (req *http.Request, err error) {
	return NewAuthenticatedRequest(instance, method, fmt.Sprintf("/api/v4/%s", endpoint), body)
}

func NewAuthenticatedRequest(instance *config.Instance, method string, path string, body io.Reader) (req *http.Request, err error) {
	if instance == nil || path == "" {
		err = ErrNoInstance
		return
	}

	if path == "" {
		path = "/"
	}
	var url string
	if strings.HasPrefix(path, "https://") || strings.HasPrefix(path, "http://") {
		url = path
	} else {
		url = fmt.Sprintf("https://%s%s", instance.Host, path)
	}

	req, err = http.NewRequest(method, url, body)
	if err != nil {
		return
	}
	req.Header.Add("PRIVATE-TOKEN", instance.Token)
	return
}

func NewAuthenticatedJSONAPIRequest(instance *config.Instance, method string, endpoint string, payload interface{}, returnVal interface{}) (resp *http.Response, err error) {
	return NewAuthenticatedJSONRequest(instance, method, fmt.Sprintf("/api/v4/%s", endpoint), payload, returnVal)
}

func NewAuthenticatedJSONRequest(instance *config.Instance, method string, path string, payload interface{}, returnVal interface{}) (resp *http.Response, err error) {
	var jsonPayload []byte
	var req *http.Request

	if instance == nil {
		err = ErrNoInstance
		return
	}

	if method == "POST" || method == "PUT" {
		if payload != nil {
			jsonPayload, err = json.Marshal(payload)
			if err != nil {
				return
			}
		}
		req, err = NewAuthenticatedRequest(instance, method, path, bytes.NewReader(jsonPayload))
		if err != nil {
			return
		}
		req.Header.Add("Content-Type", "application/json")
	} else {
		req, err = NewAuthenticatedRequest(instance, method, path, nil)
		if err != nil {
			return
		}
	}

	resp, err = http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	jsonPayload, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(jsonPayload, &returnVal)
	if err != nil {
		return
	}
	return
}

/**
GETEndpoint is a helper to execute authorized GET calls to the API of the provided instance.
*/
func GETEndpointRaw(instance *config.Instance, endpoint string) (resp *http.Response, err error) {
	var req *http.Request

	req, err = NewAuthenticatedAPIRequest(instance, "GET", endpoint, nil)
	if err != nil {
		return
	}

	resp, err = http.DefaultClient.Do(req)
	return
}
