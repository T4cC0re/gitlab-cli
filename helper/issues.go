package helper

import (
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
)

type Milestone struct {
	ID          int64  `json:"id"`
	IID         int64  `json:"iid"`
	Description string `json:"description"`
	//TODO: More
}

type Issue struct {
	ID           int64     `json:"id"`
	IID          int64     `json:"iid"`
	ProjectId    int64     `json:"project_id"`
	State        string    `json:"state"`
	Title        string    `json:"title"`
	Description  string    `json:"description"`
	Author       User      `json:"author"`
	Assignees    []User    `json:"assignees,omitempty"`
	Assignee     User      `json:"assignee,omitempty"` //Deprecated on API level
	Milestone    Milestone `json:"milestone"`
	Labels       []string  `json:"labels"`
	WebURL       string    `json:"web_url"`
	Confidential bool      `json:"confidential"`
	//TODO: More
}

/**
ProjectByPath returns the project of a project path/namespace.
*/
func GetAssignedIssues(instance *config.Instance) (issues []*Issue, err error) {
	if !InstanceSupportsFeature(instance, "issues") {
		err = ErrFeature
		return
	}
	filter := "scope=assigned_to_me"
	if !InstanceSupportsFeature(instance, "issues_api_snake_case") {
		filter = "scope=assigned-to-me"
	}

	return GetIssuesByFilter(instance, filter)
}

func GetIssuesByFilter(instance *config.Instance, filter string) (issues []*Issue, err error) {
	page := 1

	for {
		var chunk []*Issue
		_, err = NewAuthenticatedJSONAPIRequest(instance, "GET", fmt.Sprintf("issues/?%s&per_page=100&page=%d", filter, page), nil, &chunk)
		if err != nil {
			return
		}
		if len(chunk) == 0 {
			break
		}
		issues = append(issues, chunk...)
		page++
	}

	for idx := range issues {
		// Because the assignee field is deprecated we want to only use the assignees field
		// But to have consistent contents on different GitLab versions we are monkey-patching
		// it here.
		if len(issues[idx].Assignees) == 0 && issues[idx].Assignee.Id != 0 {
			issues[idx].Assignees = append(issues[idx].Assignees, issues[idx].Assignee)
		}
	}

	return
}
