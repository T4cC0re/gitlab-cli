package helper

import "strings"

func ColorStatus(in string) string {
	//sedg; s/success/'${esc}'[32msuccess ✓'${esc}'[0m/g; s/created/'${esc}'[90mcreated ⊙'${esc}'[0m/g; s/pending/'${esc}'[33mpending ⊙'${esc}'[0m/; s/failed/failed ✗/g;'

	in = strings.ReplaceAll(in, "running", "\033[36mrunning ⟳\033[0m")
	in = strings.ReplaceAll(in, "success", "\033[32msuccess ✓\033[0m")
	in = strings.ReplaceAll(in, "created", "\033[90mcreated ⊙\033[0m")
	in = strings.ReplaceAll(in, "pending", "\033[33mpending ⊙\033[0m")
	in = strings.ReplaceAll(in, "failed", "\033[31mfailed ✗\033[0m")
	return in
}
