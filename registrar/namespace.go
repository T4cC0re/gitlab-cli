package registrar

import (
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"regexp"
	"strings"
)

type Namespace struct {
	subcommands    map[string]Callback
	helptexts      map[string][]string
	defaultCommand string
}

func newNamespace() *Namespace {
	var ns Namespace
	ns.subcommands = map[string]Callback{}
	ns.helptexts = map[string][]string{}

	return &ns
}

func (ns *Namespace) RegisterCommand(command string, defaultCommand bool, callback Callback, helptext ...string) {
	ns.subcommands[command] = callback
	ns.helptexts[command] = helptext
	if defaultCommand {
		ns.defaultCommand = command
	}
}

func (ns *Namespace) Call(conf *config.Config, nsArgs []string) error {
	var subcommand string
	var args []string
	if len(nsArgs) == 0 {
		subcommand = ns.defaultCommand
	} else {
		subcommand = nsArgs[0]
	}
	if subcommand == "" {
		return ErrNotFound
	}

	if len(nsArgs) > 1 {
		args = nsArgs[1:]
	}
	if callback, ok := ns.subcommands[subcommand]; ok {
		return callback(conf, args)
	} else {
		return ErrNotFound
	}
}

func (ns *Namespace) List() {
	for subcommand, _ := range ns.subcommands {
		var suffix string
		if ns.defaultCommand == subcommand {
			suffix = " (default)"
		}
		fmt.Printf("      - %s%s\n", subcommand, suffix)
		if len(ns.helptexts[subcommand]) == 0 {
			continue
		}
		for _, line := range ns.helptexts[subcommand] {
			fmt.Printf("          %s\n", line)
		}
	}
}

func manFormat(line string) string {
	var re = regexp.MustCompile(`(?m)(\$(?:[-zA-Z0-9_]+|\{[a-zA-Z0-9_]+\}))`)
	line = re.ReplaceAllString(line, "\\fB$1\\fR")

	re = regexp.MustCompile(`(?m)(^|[\(` + "`" + `]|\s)((?:https?:/|@|~|\.)?(?:[/\.][^T][^P][a-zA-Z0-9<>.,_\-\/\\]+))(?:\b|[\)` + "`" + `])`)
	line = re.ReplaceAllString(line, "$1\\fI$2\\fR")

	if strings.HasPrefix(line, "-") {
		splits := strings.SplitN(line, "\t", 2)
		return fmt.Sprintf("\\fB%s\\fR\t%s\n\n", splits[0], splits[1])
	}

	return line + "\n\n"
}

func (ns *Namespace) ListMan() {
	for subcommand, _ := range ns.subcommands {
		var suffix string
		if ns.defaultCommand == subcommand {
			suffix = " \\fI(default)\\fR"
		}
		fmt.Printf(".RS\n.IP \"\\fB%s\\fR%s\"\n", subcommand, suffix)
		if len(ns.helptexts[subcommand]) == 0 {
			continue
		}

		for _, line := range ns.helptexts[subcommand] {
			fmt.Printf(manFormat(line))
		}
		fmt.Printf(".RE\n")
		fmt.Printf(".RE\n")
	}
}
