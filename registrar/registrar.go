package registrar

import (
	"errors"
	"flag"
	"fmt"
	"gitlab.com/T4cC0re/gitlab-cli/config"
	"strings"
	"time"
)

var ErrNotFound = errors.New("subcommand not found")

var subcommands = map[string]Callback{}
var namespaces = map[string]*Namespace{}
var helptexts = map[string][]string{}
var aliases = map[string]string{}

type Callback func(*config.Config, []string) error

func RegisterSubCommand(subcommand string, callback Callback, helptext ...string) {
	subcommands[subcommand] = callback
	helptexts[subcommand] = helptext
}

func RegisterNamespace(subcommand string, helptext ...string) *Namespace {
	ns := newNamespace()
	namespaces[subcommand] = ns
	helptexts[subcommand] = helptext
	return ns
}

func Alias(alias string, subcommandOrNamespace string) {
	aliases[alias] = subcommandOrNamespace
}

func Call(conf *config.Config, subcommand string, args []string) error {
	if namespace, ok := namespaces[subcommand]; ok {
		return namespace.Call(conf, args)
	} else if callback, ok := subcommands[subcommand]; ok {
		return callback(conf, args)
	} else if alias, ok := aliases[subcommand]; ok {
		return Call(conf, alias, args)
	} else {
		return ErrNotFound
	}
}

func aliassuffix(search string) (suffix string) {
	var cmdAliases []string
	for alias, scmd := range aliases {
		if scmd == search {
			cmdAliases = append(cmdAliases, alias)
		}
	}
	if len(cmdAliases) > 0 {
		suffix = " aliases: " + strings.Join(cmdAliases, ", ")
	}
	return
}

func List() {
	fmt.Printf("\nSubcommands:\n")
	for subcommand, _ := range subcommands {
		suffix := aliassuffix(subcommand)
		fmt.Printf("  - %s%s\n", subcommand, suffix)
		if len(helptexts[subcommand]) == 0 {
			continue
		}
		for _, line := range helptexts[subcommand] {
			fmt.Printf("      %s\n", line)
		}

	}
	fmt.Printf("\nNamespaces:\n")
	for name, namespace := range namespaces {
		suffix := aliassuffix(name)
		fmt.Printf("  - %s%s\n", name, suffix)
		if len(helptexts[name]) > 0 {
			for _, line := range helptexts[name] {
				fmt.Printf("      %s\n", line)
			}
		}
		fmt.Printf("      Commands:\n")
		namespace.List()
	}
}

func manAliassuffix(search string) (suffix string) {
	var cmdAliases []string
	for alias, scmd := range aliases {
		if scmd == search {
			cmdAliases = append(cmdAliases, fmt.Sprintf("\\fI%s\\fR", alias))
		}
	}
	if len(cmdAliases) > 0 {
		suffix = "\\fRaliases:\\ " + strings.Join(cmdAliases, " \", \"")
	}
	return
}

func GenManpage(version string) {
	fmt.Printf(
		".\\\" Manpage for gitlab-cli.\n"+
			".TH man 1 \"%s\" \"%s\" \"gitlab-cli man page\"\n"+
			".SH NAME\n"+
			"gitlab-cli \\- Perform GitLab actions on the CLI\n"+
			".SH SYNOPSIS\n"+
			".B gitlab-cli\n"+
			"[\\fI<global-flags>\\fR] \\fB<subcommand/namespace/alias>\\fR [\\fI<subcommand>\\fR] [\\fI<flags>\\fR]\n"+
			".SH DESCRIPTION\n"+
			".B gitlab-cli\n"+
			"is a command line tool to perform various actions in GitLab from the comfort of the terminal.\n"+
			".SH GLOBAL FLAGS\n",
		time.Now().UTC().Format(time.RFC3339)[:10],
		version,
	)

	flag.VisitAll(func(f *flag.Flag) {
		printVal := fmt.Sprintf("=\\fI %s\\fR", f.DefValue)
		if f.DefValue == "false" || f.DefValue == "" {
			printVal = ""
		}
		fmt.Printf(
			".TP\n"+
				".BR \\-%s\\t%s\n"+
				"%s\n",
			f.Name, printVal, f.Usage,
		)
	})

	fmt.Printf(".SH SUBCOMMANDS\n")
	for subcommand, _ := range subcommands {
		suffix := manAliassuffix(subcommand)
		fmt.Printf(
			".TP\n"+
				"\\fB%s\\fR\\t%s\n",
			subcommand, suffix)
		if len(helptexts[subcommand]) == 0 {
			continue
		}
		for _, line := range helptexts[subcommand] {
			fmt.Printf(manFormat(line))
		}
	}

	fmt.Printf(".SH NAMESPACES\n")
	for name, namespace := range namespaces {
		suffix := manAliassuffix(name)
		fmt.Printf(
			".SS %s\\t%s\n",
			name, suffix)
		if len(helptexts[name]) == 0 {
			namespace.ListMan()
			continue
		}
		fmt.Printf(".TP\n")
		for _, line := range helptexts[name] {
			fmt.Printf("%s\n", line)
		}
		namespace.ListMan()
	}

	fmt.Printf(
		manFormat(".SH CONFIGURATION\n" +
			"Put a `.gitlab-cli.yml` somewhere above your repositories. You can also put it in $HOME (~/.gitlab-cli.yml).\n\n" +
			"You can set a default description to use in merge requests, when the commit's description is empty.\n" +
			"To override the built-in default, set the default_description key to the text you like.\n" +
			"Each instance can have an alias defined, this is a user-friendly name.\n" +
			"If that is not defined, the host will be set as the alias.\n\n" +
			"The host property will be used to match repositories against, as well as for API communication.\n" +
			"Should alt_hosts be defined, these will only be used for matching.\n\n" +
			"Each instance requires to have a token set for authentication purposes.\n\n" +
			"You can create one at https://<your_gitlab>/profile/personal_access_tokens.\n\n" +
			"You can configure as many GitLab instances as you like. These will be used to match against repositories you use GitLab-CLI in, as well as for global API actions such as listing assigned issues, etc.\n\n" +
			"You can also override the global default merge request description, by setting the default_description in the instance.\n" +
			".TP\nIt should have the following YAML structure:\n\n" +
			"default_description: \"My default MR text\"\n\n" +
			"instances:\n" +
			"  - host: gitlab.com\n" +
			"    token: YOUR_GITLAB_COM_API_TOKEN\n\n" +
			"  - alias: other\n" +
			"    host: another.gitlab.net\n" +
			"    token: YOUR_OTHER_TOKEN\n" +
			"    default_description: |\n" +
			"      My complex description template.\n" + 
			"      You can also use YAML magic here\n" +
			"    alt_hosts:\n" +
			"      - ssh.another.gitlab.net\n",
		),
	)

	fmt.Printf(
		manFormat(
			".SH EXAMPLES\n" +
				".TP\ngitlab-cli request -get https://gitlab.com/api/v4/version\n" +
				"will use an HTTP GET request to fetch /api/v4/version from https://gitlab.com\n" +
				".TP\ngitlab-cli -C ~/repos/example pipeline status -remote\n" +
				"will change directories to ~/repos/example, prior to fetching the remote pipeline status\n" +
				".TP\ngitlab-cli pipeline run -var FOOBAR=baz\n" +
				"will run a new pipeline on the current branch. The pipeline will also have the variable $FOOBAR set to 'baz'\n" +
				".TP\ngitlab-cli markdown -in ./README.md -preview firefox\n" +
				"will render ./README.md in GitLab Flavoured Markdown to HTML via the API and open the temporary file with firefox\n" +
				".TP\ngitlab-cli lint\n" +
				"will lint the .gitlab-ci.yml in the root of the current git project\n" +
				".TP\ngitlab-cli mergerequest new\n" +
				"will fetch the latest commit and present it's title and description in $EDITOR. After saving and quitting $EDITOR, you will be prompted to confirm the merge request details.\n" +
				".TP\ngitlab-cli my issues\n" +
				"will show currently open issues, that are assigned to you, on all configured GitLab instances\n" +
				".TP\ngitlab-cli instances\n" +
				"will show you the alias, host and logged in user of all configured GitLab instances\n",
		),
	)
}
