# GitLab-CLI ![build status](https://gitlab.com/T4cC0re/gitlab-cli/badges/master/pipeline.svg)

__gitlab-cli__ is a command line tool to perform various actions in GitLab from the comfort of the terminal.

###### Disclaimer: While I am an employee of GitLab, this is *NOT* (yet?) an official GitLab tool!

### Releases

- [GitLab-CLI downloads](https://gitlab.com/T4cC0re/gitlab-cli/-/releases)

### Configuration & Usage

The general usage is `gitlab-cli [<global-flags>] <subcommand/namespace/alias> [<subcommand>] [<flags>]` for more details please refer to the manpages. (`man gitlab-cli`)  
If you do not have the package version, you can access the generated manpage via `man <(gitlab-cli manpage)`  
An HTML version is available for download on the [releases page](https://gitlab.com/T4cC0re/gitlab-cli/-/releases) or [viewable online](https://t4cc0re.gitlab.io/gitlab-cli/). Note, that the HTML version of the man page does not render the YAML example correctly!

##### Usage as docker container

As gitlab-cli is also provided as a container image you can also use it by invoking it. Here is an example:

```
docker run --rm -it -u $UID \
 -v $(pwd)/:/repo \
 -v ~/.gitlab-cli.yml:/home/gitlab-cli/.gitlab-cli.yml:ro \
 -v ~/.ssh/:/home/gitlab-cli/.ssh/:ro \
 -v ~/.gitconfig:/home/gitlab-cli/.gitconfig \
 registry.gitlab.com/t4cc0re/gitlab-cli:latest -C /repo
```

This will:

* Mount your repository folder as /repo into the container. `-C /repo` is required as a flag to `gitlab-cli`.
* Mount your .gitlab-cli.yml into the container's $HOME (/home/gitlab-cli/) or provide it in the repository folder.
* Make sure that files are accessible by the container user. You can overwrite the user in the container with -u $UID:$neededGroupID if necessary.
* If you are using git ssh checkouts, please also make sure to add your ssh keys and .gitconfig to the mounts.

### License

GPLv3
